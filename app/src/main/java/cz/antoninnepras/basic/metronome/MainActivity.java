package cz.antoninnepras.basic.metronome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;

import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.WindowInsets;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    boolean active;
    int bpm;
    int bpmMin;
    int bpmMax;
    int bpmStep;

    TextView bpmTv;
    Button minusBtn, plusBtn, startStopBtn;
    ImageView metronomePin;
    CountDownTimer animationTimer;
    SpringAnimation animation;
    MediaPlayer mediaPlayer;

    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setWindowProperties();


        // bpm vars
        active = false;
        bpm = 60;
        bpmMin = 20;
        bpmMax = 200;
        bpmStep = 5;
        count = 0;

        // views
        bpmTv = findViewById(R.id.bpm_tv);
        minusBtn = findViewById(R.id.minusBtn);
        plusBtn = findViewById(R.id.plusBtn);
        startStopBtn = findViewById(R.id.start_stop_btn);
        metronomePin = findViewById(R.id.metronome_view);

        minusBtn.setOnClickListener(this);
        plusBtn.setOnClickListener(this);
        startStopBtn.setOnClickListener(this);

        // animation
        animation = new SpringAnimation(metronomePin, DynamicAnimation.ROTATION, 0);

        // timer
        updateAnimationTimer();

        // sound
        mediaPlayer = MediaPlayer.create(this, R.raw.metronome_click);

        // display bpm on create
        updateBpmView();
    }

    void setWindowProperties() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    void updateAnimationTimer() {
        if (animationTimer != null) {
            animationTimer.cancel();
        }
        animationTimer = new CountDownTimer(Long.MAX_VALUE, (int)(1.0 / (float)bpm * 60*1000)) {
            @Override
            public void onTick(long millisUntilFinished) {
                count++;
                if (count % 2 == 0) {
                    animation.animateToFinalPosition(-20);

                } else {
                    animation.animateToFinalPosition(20);
                }
                mediaPlayer.start();
            }

            @Override
            public void onFinish() {
                start();
            }
        };

        animation.getSpring().setStiffness(bpm/5.0f);

        if (active) {
            animationTimer.start();
        }
    }

    void updateBpmView() {
        bpmTv.setText(getResources().getString(R.string.bpm_str, bpm));
    }

    void increaseBpm() {
        if (bpm + bpmStep > bpmMax) {
            return;
        }

        bpm += bpmStep;
        updateBpmView();
        updateAnimationTimer();
    }

    void decreaseBpm() {
        if (bpm - bpmStep < bpmMin) {
            return;
        }

        bpm -= bpmStep;
        updateBpmView();
        updateAnimationTimer();
    }

    void startStop() {
        active = !active;

        if (active) {
            startStopBtn.setText(getResources().getString(R.string.stop_btn_str));
            animationTimer.start();
        } else {
            startStopBtn.setText(getResources().getString(R.string.start_btn_str));
            animationTimer.cancel();
        }
    }

    @Override
    public void onClick(View v) {
        Button btn = (Button) v;
        if (btn.getId() == minusBtn.getId()) {
            decreaseBpm();
        } else if (btn.getId() == plusBtn.getId()) {
            increaseBpm();
        } else if (btn.getId() == startStopBtn.getId()) {
            startStop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setWindowProperties();
    }

}